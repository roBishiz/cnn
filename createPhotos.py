import cv2
import os

temp = ['Boris_Burcev', 'Paul_Ustinov', 'Nikita_Belyy', 'Renat_Yanmurzin', 'Nothing']
name = 'Nikita_Belyy'
name_dir = 'datasets\\record\\' + name + '\\'

if not os.path.isdir(name_dir):
     os.mkdir(name_dir)

cap = cv2.VideoCapture(0)

iter = 1

while True:
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Display the resulting frame
    cv2.imshow('frame', frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        writePath = name_dir + name + '_' + str(iter) + '.png'
        cv2.imwrite(writePath, frame)
        iter += 1

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()