from matplotlib import pyplot
from PIL import Image
from numpy import asarray
from mtcnn.mtcnn import MTCNN

folder = 'datasets\\record'


def extract_face(filename, required_size=(224, 224)):
    # load image from file
    pixels = pyplot.imread(filename)
    # create the detector, using default weights
    detector = MTCNN()
    # detect faces in the image
    results = detector.detect_faces(pixels)
    # extract the bounding box from the first face
    x1, y1, width, height = results[0]['box']
    x2, y2 = x1 + width, y1 + height
    # extract the face
    face = pixels[y1:y2, x1:x2]
    # resize pixels to the model size
    image = Image.fromarray(face)
    image = image.resize(required_size)
    image.save(filename)


extract_face('datasets\\Aaron_Eckhart\\Aaron_Eckhart_0001.jpg')

# for subdir, dirs, files in os.walk(folder):
#     for file in files:
#         filePath = subdir + '\\' + file
#         print(filePath)
#         extract_face(filePath)
    # print(subdir)
    # print(dirs)
    # print(files)


# extract_face('datasets\\trainForMiniVisualizing\\')