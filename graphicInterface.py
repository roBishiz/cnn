from PyQt5 import QtCore, QtGui
from PyQt5.QtWidgets import QWidget, QApplication, QLabel, QPushButton, QGridLayout, QMenuBar, QMenu
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import pyqtSignal, pyqtSlot, Qt, QThread, QTimer, QRect
import sys
import cv2
import numpy as np
from keras.models import load_model
from PIL import Image, ImageOps
import os

face_cascade_db = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml")


class VideoThread(QThread):
    change_pixmap_signal = pyqtSignal(np.ndarray)

    def __init__(self):
        super().__init__()
        self._run_flag = True

    def run(self):
        # capture from web cam
        cap = cv2.VideoCapture(0)
        while self._run_flag:
            ret, cv_img = cap.read()
            img_gray = cv2.cvtColor(cv_img, cv2.COLOR_BGR2GRAY)
            faces = face_cascade_db.detectMultiScale(img_gray, 1.1, 19)
            for (x, y, w, h) in faces:
                cv2.rectangle(cv_img, (x, y), (x + w, y + h), (0, 255, 0), 2)
            if ret:
                self.change_pixmap_signal.emit(cv_img)
        # shut down capture system
        cap.release()

    def stop(self):
        """Sets run flag to False and waits for thread to finish"""
        self._run_flag = False
        self.wait()


class App(QWidget):
    def __init__(self):
        super().__init__()

        self.menuBar = QMenuBar()
        self.fileMenu = QMenu()
        self.menuBar.addMenu(self.fileMenu)
        self.fileMenu.addAction('Сфотографировать')

        self.setWindowTitle("CNNRecognition")
        self.disply_width = 640
        self.display_height = 480

        self.image_label = QLabel(self)
        self.image_label_recognition = QLabel(self)
        self.textLabel = QLabel('1')

        self.buttonSkip = QPushButton(self)
        self.buttonSkip.setText("Пропустить")

        # self.buttonTakePhoto = QPushButton(self)
        # self.buttonTakePhoto.setText("Сфотографировать")

        gbox = QGridLayout()
        gbox.addWidget(self.image_label, 0, 0, 1, 1)
        gbox.addWidget(self.image_label_recognition, 0, 1, 1, 1)
        gbox.addWidget(self.buttonSkip, 1, 1, 1, 1)
        # gbox.addWidget(self.buttonTakePhoto, 1, 1, 1, 1)
        gbox.addWidget(self.textLabel, 1, 0, 1, 1)
        gbox.setMenuBar(self.menuBar)
        self.setLayout(gbox)

        self.model = load_model('model2.h5')
        self.size = (224, 224)
        self.data = np.ndarray(shape=(1, 224, 224, 3), dtype=np.float32)

        self.classes = []
        for dirpath, dirnames, _ in os.walk("datasets\\train\\train"):
            for dirname in dirnames:
                self.classes.append(dirname.split()[-1])

        self.thread = VideoThread()
        self.thread.change_pixmap_signal.connect(self.update_image)
        self.thread.start()

        self.cv_image = Image.open('datasets\\trainTest\\test\\Ali_Naimi\\Ali_Naimi_0003.jpg')
        self.cv_image = ImageOps.fit(self.cv_image, self.size, Image.ANTIALIAS)
        self.cv_image = np.asarray(self.cv_image)

        self.timer = QTimer()
        self.timer.timeout.connect(self.slotTimerAlarm)
        self.timer.start(1000)

    def openImage(self, name):
        # print('paht' + os.listdir('train\\trainTest\\train\\' + name)[0])
        pixmap = QPixmap('datasets\\train\\recognition\\' + name + '\\' + os.listdir('datasets\\train\\recognition\\' + name)[0])
        self.image_label_recognition.setPixmap(pixmap)

    def closeEvent(self, event):
        self.thread.stop()
        event.accept()

    @pyqtSlot(np.ndarray)
    def update_image(self, cv_img):
        """Updates the image_label with a new opencv image"""
        qt_img = self.convert_cv_qt(cv_img)
        self.cv_image = cv_img
        # rect = QRect(320, 240, 300, 300)
        # qt_img = qt_img.copy(rect)
        self.image_label.setPixmap(qt_img)

    def slotTimerAlarm(self):
        cv2.imwrite('buf.jpeg', self.cv_image)

        image_realise = Image.open('buf.jpeg')
        image_realise = ImageOps.fit(image_realise, self.size, Image.ANTIALIAS)
        image_array = np.asarray(image_realise)
        normalized_image_array = (image_array.astype(np.float32) / 127.0) - 1
        self.data[0] = normalized_image_array

        y_prob = self.model.predict(self.data)
        print(y_prob)
        # squeeze_prob = np.squeeze(y_prob)
        # print(squeeze_prob)
        # print(squeeze_prob.shape)

        for i in range(y_prob[0].size):
            if y_prob[0][i] > 0.9:
                self.openImage(self.classes[i])
                self.textLabel.setText(self.classes[i])
                print(self.classes[i])

    def convert_cv_qt(self, cv_img):
        """Convert from an opencv image to QPixmap"""
        rgb_image = cv2.cvtColor(cv_img, cv2.COLOR_BGR2RGB)
        h, w, ch = rgb_image.shape
        bytes_per_line = ch * w
        convert_to_Qt_format = QtGui.QImage(rgb_image.data, w, h, bytes_per_line, QtGui.QImage.Format_RGB888)
        p = convert_to_Qt_format.scaled(self.disply_width, self.display_height, Qt.KeepAspectRatio)
        return QPixmap.fromImage(p)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    a = App()
    a.show()
    sys.exit(app.exec_())
